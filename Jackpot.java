
public class Jackpot
{
	public static void main(String[] args)
	{
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		System.out.println("Welcome to Jackpot!");
		Board board = new Board();
		
		while (gameOver == false)
		{
			System.out.println(board);
			if (board.playATurn())
			{
				gameOver = true;
			}
			else
			{
				numOfTilesClosed += 1;
			}
		}
		if (numOfTilesClosed >= 7)
		{
			System.out.println("YOU JACKPOTTED");
		}
		else
		{
			System.out.println("Damn, you lost. Better luck next time!");
		}
	}
}