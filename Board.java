
public class Board
{
	private boolean[] tiles;
	private Die dieA;
	private Die dieB;
	
	public Board()
	{
		this.dieA = new Die();
		this.dieB = new Die();
		this.tiles = new boolean[12];
		for(int i = 0; i< this.tiles.length; i++)
		{
			this.tiles[i] = false;
		}
	} 
	
	public String toString()
	{
		String output = "";
		
		for(int i = 0; i< this.tiles.length; i++)
		{
			if (this.tiles[i])
			{
				output = output + " X";
			}
			else
			{
				output = output + " " + (i + 1);
			}
		}
		
		return output;
	}
	
	public boolean playATurn()
	{
		this.dieA.roll();
		this.dieB.roll();
		System.out.println(this.dieA);
		System.out.println(this.dieB);
		
		int sumOfDice = this.dieA.getFaceValue() + this.dieB.getFaceValue();
		
		if (!this.tiles[(sumOfDice-1)])
		{
			this.tiles[(sumOfDice-1)] = true;
			System.out.println("Closing tile equal to sum: " + (sumOfDice ));
			return false;
		}
		else
		{
			if (!this.tiles[(this.dieA.getFaceValue()-1)])
			{
				this.tiles[(this.dieA.getFaceValue()-1)] = true;
				System.out.println("Closing tile with the same value of die A: " + (this.dieA.getFaceValue()));
				return false;
			}
			else if (!this.tiles[(this.dieB.getFaceValue()-1)])
			{
				this.tiles[(this.dieB.getFaceValue()-1)] = true;
				System.out.println("Closing tile with the same value of die B: " + (this.dieB.getFaceValue()));
				return false;
			}
			else
			{
				System.out.println("All the tiles for these values are already shut.");
				return true;
			}
		}
	}
}